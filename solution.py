import json


def main():
    file = open("input", "r")
    for line in file:

        dictionary = {
            "satelliteId": 0000,
            "severity": "",
            "component": "",
            "timestamp": ""
        }

        satellite_id = ""
        severity = ""
        component = ""
        timestamp = ""
        red_high = ""
        yellow_high = ""
        yellow_low = ""
        red_low = ""
        raw_value = ""
        count_vertical_line = 0

        # parsing through each line and separating what is necessary
        for i, c in enumerate(line):
            if count_vertical_line == 0:
                if c == " ":
                    timestamp += "T"
                elif c == "|":
                    count_vertical_line += 1
                    timestamp += "Z"
                else:
                    timestamp += c

            elif count_vertical_line == 1:
                if c == "|":
                    count_vertical_line += 1
                else:
                    satellite_id += c

            elif count_vertical_line == 2:
                if c == "|":
                    count_vertical_line += 1
                else:
                    red_high += c

            elif count_vertical_line == 3:
                if c == "|":
                    count_vertical_line += 1
                else:
                    yellow_high += c

            elif count_vertical_line == 4:
                if c == "|":
                    count_vertical_line += 1
                else:
                    yellow_low += c

            elif count_vertical_line == 5:
                if c == "|":
                    count_vertical_line += 1
                else:
                    red_low += c

            elif count_vertical_line == 6:
                if c == "|":
                    count_vertical_line += 1
                else:
                    raw_value += c

            elif count_vertical_line == 7:
                component += c

        # determining severity
        if float(raw_value) >= int(red_high) or float(raw_value) >= int(red_high) - 2:
            severity += "RED HIGH"
        elif float(raw_value) >= int(yellow_high) - 2 and float(raw_value) <= int(red_high) - 2:
            severity += "YELLOW HIGH"
        elif float(raw_value) <= int(yellow_low) - 2 and float(raw_value) > int(red_low):
            severity += "YELLOW LOW"
        else:
            severity += "RED LOW"

        # cleaning how it looks
        component = component.replace("\n", "")

        # add values to dictionary
        dictionary["satelliteId"] = int(satellite_id)
        dictionary["severity"] = severity
        dictionary["component"] = component
        dictionary["timestamp"] = timestamp

        # turn dictionary into JSON
        create_json = json.dumps(dictionary)
        print(create_json)


main()

